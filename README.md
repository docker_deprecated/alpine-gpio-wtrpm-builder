# alpine-gpio-wtrpm-builder

#### [alpine-aarch64-gpio-wtrpm-builder](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-gpio-wtrpm-builder/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64build/alpine-aarch64-gpio-wtrpm-builder/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64build/alpine-aarch64-gpio-wtrpm-builder/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64build/alpine-aarch64-gpio-wtrpm-builder/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64build/alpine-aarch64-gpio-wtrpm-builder)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64build/alpine-aarch64-gpio-wtrpm-builder)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : aarch64
* Appplication : [WtRPM](http://www.mupuf.org/blog/2013/05/11/wtrpm-a-web-based-wt-suite-to-power-up-slash-down-your-computers/)
    - A Web-based (Wt) Suite to Power Up/down Your Computers.



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

